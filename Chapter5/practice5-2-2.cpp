#include "stdio.h" //高精度计算，求n！
#include "string.h"
const int maxn=3000 ;
int a[maxn];
int main(){
	int i,j,n;
	scanf("%d",&n);
	printf("计算:%d！\n",n);
	memset(a,0,sizeof(a));//将数组a初始化为全0
	a[0]=1 ;
	for (i=2; i<=n ;i++ )
	{
		int temp=0;//存储进位值
		for (j=0;j<maxn ;j++ )
		{
			int sum=a[j]*i+temp;//模拟手工计算中的一个乘数乘以一个位数
			a[j]=sum%10;
			temp=sum/10;
		}
	}
	for(i=maxn-1;i>=0;i--) if(a[i]) break;
	for(j=i;j>=0;j--) printf("%d",a[j]);
	printf("\n");
	return 0;
}