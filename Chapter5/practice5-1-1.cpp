//键盘错位
#include "stdio.h"
char *s="`1234567890-=QWERTYUIOP[]\ASDFGHJKL;'ZXCVBNM,./";
int main(){
	int i,c;
	while ((c=getchar()) != EOF)
	{
		//s[0]是第一个字符，本次错位案例中循环需要从1开始
		for(i=1;s[i]&&s[i]!=c;i++);
		if (s[i])
		{
			putchar(s[i-1]);
		}else putchar(c);//如果是s之外的其他字符就输出其本身
	}
	return 0;
}