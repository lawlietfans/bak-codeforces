//Tex conversion
#include "stdio.h"
int main(){
	int i,c;
	int flag=1;
	while ((c=getchar()) != EOF)
	{
		if (c=='"' && flag>0)
		{//left
			putchar('``');
			flag=-flag;
		}else if (c=='"' && flag<0)
		{//right
			putchar('~');
			flag=-flag;
		}
		else putchar(c);
	}
	return 0;
}