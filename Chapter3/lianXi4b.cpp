#include <stdio.h>
#include <string.h>
int main(){
	//char c=getchar();
	char s[10];
	sprintf(s,"12:22:39");
	
	printf("%s\n",s);
	int HH=0,MM=0,SS=0;
	sscanf(s,"%d:%d:%d",&HH,&MM,&SS);
	printf("%d-%d-%d\n",HH,MM,SS);
	return 0;
}
/*http://see.xidian.edu.cn/cpp/html/296.html
format参数有些类似正则表达式（当然没有正则表达式强大，复杂字符串建议使用正则表达式处理），支持集合操作，例如：
    %[a-z] 表示匹配a到z中任意字符，贪婪性(尽可能多的匹配)
    %[aB'] 匹配a、B、'中一员，贪婪性
    %[^a] 匹配非a的任意字符，贪婪性

另外，format不仅可以用空格界定字符串，还可以用其他字符界定，可以实现简单的字符串分割（更加灵活的字符串分割请使用strtok()）。例如：
    sscanf("2006:03:18", "%d:%d:%d", a, b, c);
    sscanf("2006:03:18 - 2006:04:18", "%s - %s", sztime1, sztime2);
*/