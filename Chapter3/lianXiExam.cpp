#include <stdio.h>
#include<string.h>
int main()
{
//函数声明
	//void stat();
	//void stat2();
	//void calculator();
	void rotate();
//函数调用
	//stat();
	//stat2();
	//calculator();
	rotate();
    return 0;
}
//分数统计
//http://acm.dlut.edu.cn/problem.php?id=1132
void stat(){
	int LENTH=105;
	int a[LENTH];
	int n,score,max;
    int i;
    while(scanf("%d",&n),n)
    {
        memset(a,0,sizeof(a));//sizeof(a)=420
        for(i=0;i<n;i++)//input n number
        {
            scanf("%d",&score);
            a[score]++;
        }
		max=a[0];
        for (i=0;i<LENTH ;i++ )
        	if(max<a[i]) max=a[i];
		//output
		//printf("max=%d\n",max);
		for (i=0;i<LENTH ;i++ )
		{
			if (a[i]==max)
			{
				printf("%d ",i);
			}
		}
		printf("\n");
    }   
}
//stat2:float version(两位小数)
void stat2(){
	int LENTH=10000;
	int a[LENTH];
	int n,newScore,max,i;
	double score;
    while(scanf("%d",&n),n)
    {
        memset(a,0,sizeof(a));//sizeof(a)=420
        for(i=0;i<n;i++)//input n number
        {
            scanf("%lf",&score);
			newScore=(int)(score*100);
            a[newScore]++;
        }
		max=a[0];
        for (i=0;i<LENTH ;i++ )
        	if(max<a[i]) max=a[i];
		//output
		//printf("max=%d\n",max);
		for (i=0;i<LENTH ;i++ )
		{
			if (a[i]==max)
			{
				printf("%.2lf ",i/100.0);
			}
		}
		printf("\n");
    }   
}
//计算器
void calculator(){
	int result=123456;
	int a,b;
	char c;
	char s[100];
	sprintf(s,"1112+  1300");
	printf("%s\n",s);
	sscanf(s,"%d%[+|-|*]%d",&a,&c,&b);
	if (c=='+')
	{
	 printf("%d\n",(a+b)%1000);

	}
}
//题目要求：输入一个n*n字符矩阵，把它左转90度后输出 
/*
样例输入：
3
abcdefghi 
样例输出：
a b c
d e f
g h i

c f i
b e h
a d g 
*/ 
void rotate(){
	
	int n,i,j,length=105;
	char temp,s[length][length];
	scanf("%d",n);
	//n=3;
	for (i=0;i<n ;i++ )
	{
		for (j=0;j<n ;j++ )
		{
			scanf("%c",&temp);
			s[i][j]=temp;
			printf("%d-%d-%c\n",i,j,temp);
		}
	}
	//左转90度需要两步：1、对折；2、纵轴互补；
	char change[length][length];
	for (i=0;i<n ;i++ )
	{
		for (j=0;j<n ;j++ )
		{
			change[i][j]=s[j][i];
		}
	}
	for (i=0;i<n ;i++ )
	{
		for (j=0;j<n ;j++ )
		{
			//s[i][j]=change[i][n-1-j];//操作列的次序表示右旋90度
			s[i][j]=change[n-1-i][j];//，操作行的次序表示左旋90度
			printf("%c ",s[i][j]);
		}
		printf("\n");
	}
}
//进制转换1
void base1(){}
//进制转换2
void base2(){}
//手机键盘
void keyboard(){}