#include "stdio.h"
int VAR=0;//定义全局变量
int main(){
	int * problem3();
	int *p;
	p=problem3();
	printf("%d,%d\n",p,*p);
	return 0;
}
//problem1:编写函数solve求解方程组
/*两直线重合，多个解
两直线平行，无解
其他情况，相交有一个解，公式求出*/
//problem2:修改全局变量值
/*main中调试语句，结果为4,8
int f();int g();int h();
	int a,b;
	a=(f()+g())+h();
	b=f()+(g()+h());
	printf("%d,%d\n",a,b);
*/
int f(){
	VAR++;
	return VAR;
}
int g(){ return f()+1;}
int h(){ return 0;}
//problem3:局部变量、全局变量
/*两者可以重名，输出为-1，main中调试语句：
int VAR=-1;
	printf("%d\n",VAR);
*/
int * problem3(){
	int a=-100;
	return &a;
}
/*编译时会有warning: address of local variable `a' returned
main中调试语句：
int * problem3();
	int *p;
	p=problem3();//若main中声明problem3()为int类型，则此处需要强制转换为int*型；
	printf("%d,%d\n",p,*p);
*/
//problem4: use size command
//WE NEED GDB TOOL!