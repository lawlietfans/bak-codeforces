#include "stdio.h"//孪生素数2
#include "math.h"
#include "assert.h"
int is_prime(int x){
	int i,m;
	assert(x>=0);//合理使用assert宏
	if(x==1) return 1;
	m = floor(sqrt(x)+0.5);
	for (i=2;i<=m ;i++ )
	{
		if (x%i == 0) return 0;//不是除法
	}
	return 1;
}
int main(){
	int i,m;
	scanf("%d",&m);
	for (i=m;i>2 ;i-- )
	{
		if (i%2!=0&&is_prime(i-2)&&is_prime(i))
		{
			printf("%d %d\n",i-2,i);
			break;
		}
	}
	return 0;
}