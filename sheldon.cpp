#include <stdio.h>
#define maxn 50
int a[maxn];
int main()
{
    int i,j,t,n,s=1;
	//测试个数
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d",&n);
		//从1开始数
        for(i=1;i<=n;i++) scanf("%d",&a[i]);
		//s记录第几次
        printf("Case %d:\n",s++);
		int thisSum,maxSum,begin,end;
        maxSum=a[1];
		begin=end=1;
		for (i=1;i<=n ;i++ )
		{
			thisSum=0;//this line is important
			for (j=i;j<n ;j++ )
			{
				thisSum=thisSum+a[j];
				if (thisSum>maxSum)
				{
					maxSum=thisSum;
					begin=i;
					end=j;
				}
			}
		}
        printf("%d %d %d\n",maxSum,begin,end);
        if(t!=0) printf("\n");
    }
    return 0;
}