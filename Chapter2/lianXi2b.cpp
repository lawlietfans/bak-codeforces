//data count,redirect version
#define LOCAL
#include <stdio.h>
#define INF 999999

int main(){
#ifdef LOCAL
	freopen("data.in","r",stdin);
	freopen("data.out","w",stdout);
#endif
	int x,MAX=-INF,MIN=INF,n=0,s=0;
	while (scanf("%d",&x)==1)
	{
		if(x<=MIN) MIN=x;
		if(x>=MAX) MAX=x;
		s=s+x;
		/*
		printf("x=%d,min=%d,max=%d\n",x,MIN,MAX);
		*/
		n++;
	}
	printf("max=%d,min=%d,a=%f\n",MAX,MIN,(float)s/n);
	return 0;
}