#include <stdio.h>
int main(){
	//声明函数
		//void digit();
		//void hanxin();
		//void triangle();
		void subsequence();
	//调试
		//digit();
		//hanxin();
		//triangle();
		subsequence();
	return 0;
}
//位数
void digit(){
	long long int n,highDigit;
	scanf("%lld",&n);
	int mod=10;
	int result=1;
	highDigit = n-n%mod;
	while (highDigit>0)
	{
		mod=mod*10;
		result++;
		highDigit = n-n%mod;
	}
	printf("%d\n",result);
}
//韩信点兵
void hanxin(){
	int a,b,c;
	int i;
	scanf("%d%d%d",&a,&b,&c);
	for (i=10;i<101;i++ )
	{
		if (i%3==a && i%5==b && i%7==c) break;
	}
	if(i>100) printf("No answer\n");
	else printf("%d\n",i);
}
//倒三角形
void triangle(){
	int i,j,n;
	scanf("%d",&n);
	for (i=n;i>0 ;i-- )
	{
		for (j=0;j<n-i;j++) printf(" ");
		for (j=0;j<2*i-1 ;j++) printf("#");
	printf("\n");
	}
}
//子序列的和
void subsequence(){
	long long int i,n,m;	
	float result=0;
	scanf("%lld%lld",&n,&m);
	for (i=n;i<=m ;i++ )
	{
		result+=1.0/(i*i);
		//if (1.0/(i*i)<0.000000001) break;
		printf("%.31f\n",1.0/(i*i));
	}
	printf("%.5f\n",result);
}
/*
测试数据为65536 655360
但1/65536=0.0000000002328306436538696300000
最终结果为0.00000而非0.00001
*/