//factorial sum
/*
line18显示i!的值，line16如果不取余会溢出
CMD下测试：
F:\workspace\Codeforces\Chapter2>echo 20|lianxi2
820313
Time used = 0.021000000000000001000
*/
#include <stdio.h>
#include <time.h>
int main() {
	const int MOD = 1000000;
	int i,j,n,s=0;
	scanf("%d",&n);
	for (i=1;i<=n ;i++ )
	{
		int factorial = 1;
		for (j=1;j<=i;j++ )
		{
			factorial=factorial*j % MOD;
		}
		printf("%d!取余之后=%d\n",i,factorial);
		s=s+factorial ;
		s=s%MOD;
	}
	printf("%d\n",s);
	printf("Time used = %.21f\n",(double)clock()/CLOCKS_PER_SEC);
	return 0;
}
