//1.5小结与习题
#include <stdio.h>
#include <math.h>
//引入此头文件之后可以使用INT_MIN等常量
#include <limits.h>
int main(){
	printf("%d\n",sizeof(int));
	//通过实验找出int最小值、最大值；
	int i=1;
	while ( i>0)
	  i++;
	printf("Min=%d Max=%d\n", i, i-1);
	printf("%d %d\n",INT_MIN,INT_MAX);
	//超过一定位数，显示的就是错误的了，这样可以看出double的精确度
	printf("%.50lf\n", 1.0/3.0 );   
	return 0;
}