#include <stdio.h>
int main(){
	//函数声明
	void overage();
	void temperature();
	void sum();
	void abs();
	void year();
	//overage
	//	overage();
	//temperature
	//	temperature();
	//sum
	//	sum();
	//abs
	//	abs();
	//year
		year();
	return 0;
}
//function list
void overage(){
	int a,b,c;
	scanf("%d%d%d",&a,&b,&c);
	printf("%.3f\n",(a+b+c)/3.0);
}
void temperature(){
	float temp;
	scanf("%f",&temp);
	printf("%.3f\n",5*(temp-32)/9);
}
void sum(){
	int n;
	scanf("%d",&n);
	printf("%d\n",(1+n)*n/2);
}
void abs(){
	float temp;
	scanf("%f",&temp);
	if (temp<0)
	{
		temp=-temp;
	}
	printf("%.2f\n",temp);
}
void year(){
	int year=0;
	scanf("%d",&year);
	//四年一闰，百年不闰，四百年再闰
	if (year%4==0 && year%100!=0 || year%400==0)
	{
		printf("yes\n");
	}else {
		printf("no\n");
	}
}
